import java.util.*;

public class Zahlensortierer {

  public static void main(String[] args) {
	  int[] input = new int[10];
	  Scanner scan = new Scanner (System.in);
	  for(int i=0; i<input.length; i++) {
		  System.out.print("Geben sie die "+(i+1)+". Zahl ein: ");
		  input[i] = scan.nextInt();
	  }
	  scan.close();
	  Arrays.sort(input);
	  System.out.printf("Sortiert: "+Arrays.toString(input));
  } 
} 